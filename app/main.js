const electrin = require('electron');


//控制应用生命周期的模块
const {app}=electrin;

//创建本地浏览器窗口的模块
const {BrowserWindow}=electrin;

//指向窗口对象的一个全局引用，如果没有这个引用，那么当该javascript对象被垃圾回收的时候该窗口将会自动关闭
let win;

const path = require('path')
const url = require('url')

let openHtml = (html) => {
    win.loadURL(url.format({
        pathname: path.join(__dirname, html),
        protocol: 'file:',
        slashes: true
    }));
};

let createWindow = () => {
    //创建一个新的浏览器窗口
    win = new BrowserWindow({width: 1104, height: 620});

    //并且装载应用的index.html页面
    openHtml('index.html');

    //打开开发工具页面
    //win.webContents.openDevTools();

    //当窗口关闭时调用的方法
    win.on('closed', () => {
        //解除窗口对象的引用，通常而言如果应用支持多个窗口的话，你会在一个数组里存放窗口对象，在窗口关闭的时候应当删除对应的元素。
        win = null;
    });
};

//当electron完成初始化并且已经创建了浏览器窗口，则该方法将会被调用。
//有些API只能在该事件发生后才能被使用。
app.on(
    'ready', createWindow
);

//当所有的窗口被关闭后退出应用
app.on(
    'window-all-closed', () => {
        //对于OS X系统，应用和相应的菜单栏会一直激活直到用户通过CMD+Q显示退出
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });

app.on(
    'activate', () => {
        //对于OS X系统，当dock图标被点击后会重新创建一个app窗口，并且不会有其他窗口打开
        if (win === null) {
            createWindow();
        }
    }
);

//通信模块，main process 与 renderer process (web page)
const {ipcMain}=require('electron');

ipcMain.on('asynchronous-message', (event, arg) => {
    console.log('mian1' + arg);//prints ping
    //向页面端请求通讯
    event.sender.send('asynchronous-reply', 'pong');
});

ipcMain.on('synchronous-message', (event, arg) => {
    console.log('main2' + arg);//prints ping
    event.returnValue = 'pong';
});

const {shell} = require('electron');

//打开文件
ipcMain.on('openFile', (event, arg) => {
    shell.openItem(arg);
});

//打开网站
ipcMain.on('openUrl', (event, arg) => {
    shell.openExternal(arg);
});


const Menu = require('electron').Menu;
let template = [
    {
        label: '首页',
        click: () => {
            openHtml('index.html');
        }
    }, {
        label: '计算机',
        click: () => {
            openHtml('html/calculator/index.html');
        }
    },
    {

        label: '其他',
        submenu: [
            {
                label: 'Undo',
                accelerator: 'CmdOrCtrl+Z',
                role: 'undo',
                click: () => {
                    console.log('Ctrl+Z')
                }
            }
        ]
    }];
let menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);